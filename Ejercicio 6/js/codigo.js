let letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

const numero = prompt("Introduce los números del DNI");
const letra = prompt("Introduce la letra del DNI");

let calculo = 0;

if (numero < 0 || numero > 99999999) {

    alert("El número proporcionado no es válido");

} else {

    calculo = numero % 23;

}

if (letra !== letras[calculo]) {

    alert("La letra indicada no es correcta");

} else {

    alert("El número y la letra del DNI son correctos");

}